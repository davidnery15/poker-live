import thunk from "redux-thunk"
import rootReducer from "./reducers"
import persistState, { mergePersistedState } from "redux-localstorage"
import adapter from "redux-localstorage/lib/adapters/sessionStorage"
import { applyMiddleware, compose, createStore } from "redux"
import { connectRouter, routerMiddleware } from "connected-react-router"
import { createBrowserHistory } from "history"
// import logger from 'redux-logger'

export const history = createBrowserHistory()

const reducer = compose(
  connectRouter(history),
  mergePersistedState()
)(rootReducer)

const enhancer = compose(
  applyMiddleware(
    routerMiddleware(history),
    // logger,
    thunk
  ),
  persistState(adapter(window.sessionStorage), "wall-e")
)

export default createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  enhancer
)

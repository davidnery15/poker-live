import { db } from "../../config/firebase"
import axios from 'axios'
// LOGIN
export const USER_LOGIN_STARTED = "USER_LOGIN_STARTED"
export const USER_LOGIN_DONE = "USER_LOGIN_DONE"
export const USER_LOGIN_ERROR = "USER_LOGIN_ERROR"
//LOGOUT
export const USER_LOGOUT = "USER_LOGOUT"

//----GENERAL ACTIONS----
//LOGIN
export const loginUserUssernamePass = ({ username, password }) => {
  return async dispatch => {
    dispatch({ type: USER_LOGIN_STARTED })
    try {
      const response = await axios
        .post('https://us-central1-poker-live-42745.cloudfunctions.net/api/v1/login',
          {
            userName: username,
            password: password
          })
      if (response.status === 200) {
        db.ref(`users/${response.data.userToken}`).once("value", (snapshot) => {
          const adminCheck = snapshot.val().admin
          dispatch({
            type: USER_LOGIN_DONE,
            payload: {
              accessToken: response.data.userToken,
              adminCheck: adminCheck
            }
          })
        })
      } else {
        dispatch({
          type: USER_LOGIN_ERROR,
          payload: {
            code: "email-not-verified",
            message: "The email has not been verified"
          },
          error: true
        })
      }
    } catch (error) {
      dispatch({
        type: USER_LOGIN_ERROR,
        payload: error
      })
    }
  }
}
//LOGOUT
export const userLogout = () => dispatch => {
  dispatch({
    type: USER_LOGOUT
  })
}
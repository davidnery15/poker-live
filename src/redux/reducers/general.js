import {
  //USER LOGIN
  USER_LOGIN_STARTED,
  USER_LOGIN_DONE,
  USER_LOGIN_ERROR,
  //LOGOUT
  USER_LOGOUT
} from "../actions/general";
const initialState = {
  //GENERAL
  isLoggedIn: null,
  accessToken: null,
  adminCheck: null,
  isFetching: null,
  type: null
};
const general = (state = initialState, action) => {
  //----GENERAL REDUCERS----
  switch (action.type) {
    //USER LOGIN
    case USER_LOGIN_STARTED:
      return {
        ...state,
        isFetching: true,
        type: "USER_LOGIN_STARTED"
      };
    case USER_LOGIN_DONE:
      return {
        ...state,
        isLoggedIn: true,
        accessToken: action.payload.accessToken,
        adminCheck: action.payload.adminCheck,
        isFetching: false
      }
    case USER_LOGIN_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        isFetching: false,
        type: "USER_LOGIN_ERROR"
      }
    //USER LOGOUT
    case USER_LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        accessToken: null,
        isFetching: null,
        type: null
      }
    default:
      return state;
  }
};
export default general;

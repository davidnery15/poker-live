import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const firebaseConfig = {
  apiKey: "AIzaSyCC1JDv5yNGA3dXOvwJALQzi7W6aA0pv3w",
  authDomain: "poker-live-42745.firebaseapp.com",
  databaseURL: "https://poker-live-42745.firebaseio.com",
  projectId: "poker-live-42745",
  storageBucket: "poker-live-42745.appspot.com",
  messagingSenderId: "382796948713",
  appId: "1:382796948713:web:d6fc30b9a72deb328e7c78",
  measurementId: "G-PT6P0JHVXW"
}
firebase.initializeApp(firebaseConfig)

export default firebase
export const db = firebase.database()

import React from 'react'
import Headset from '../../assets/headset.svg'
import Phone from '../../assets/phone.svg'
import Mail from '../../assets/mail.svg'
import Chat from '../../assets/chat.svg'

function Support() {

  const openLZ = () => {
    window.open(
      'http://support.pokerdelasamericas.com/livezilla.php',
      '',
      'width=590,height=550,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'
    );
  }

  return (
    <div className="support">
      <div className="headsetSupport">
        <img src={Headset} alt="headset" />
        <p>SOPORTE</p>
      </div>
      <div className="contactSupport">
        <div className="phone">
          <img src={Phone} alt="phone" />
          <p>0424-6941674</p>
        </div>
        <div className="mail">
          <img src={Mail} alt="mail" />
          <p>soporte@pokerdelasamericas.com</p>
        </div>
        <div className="chat">
          <img src={Chat} alt="chat" />
          <button onClick={openLZ}>Chat en línea</button>
        </div>
      </div>
    </div>
  )
}

export default Support
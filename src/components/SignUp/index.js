import React, { useState } from 'react'
import { db } from '../../config/firebase'
import { toast } from 'react-toastify'
import NumberFormat from 'react-number-format';
import Options from './options'

function Signup() {
  const [name, setName] = useState('')
  const [firstLastName, setFirstLastName] = useState('')
  const [secondLastName, setSecondLastName] = useState('')
  const [bornDate, setBornDate] = useState('')
  const [email, setEmail] = useState('')
  const [emailConfirmed, setEmailConfirmed] = useState('')
  const [country, setCountry] = useState('VE')
  const [home, setHome] = useState('')
  const [postalCode, setPostalCode] = useState('')
  const [location, setLocation] = useState('')
  const [province, setProvince] = useState('')
  const [userName, setUserName] = useState('')
  const [nickname, setNickname] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const handleSetName = (e) => setName(e.target.value)
  const handleSetFirstLastName = (e) => setFirstLastName(e.target.value)
  const handleSecondLastName = (e) => setSecondLastName(e.target.value)
  const handleSetBornDate = (e) => setBornDate(e.target.value)
  const handleSetEmail = (e) => setEmail(e.target.value)
  const handleSetEmailConfirmed = (e) => setEmailConfirmed(e.target.value)
  const handleSetCountry = (e) => setCountry(e.target.value)
  const handleSetHome = (e) => setHome(e.target.value)
  const handleSetPostalCode = (e) => setPostalCode(e.target.value)
  const handleSetLocation = (e) => setLocation(e.target.value)
  const handlerSetProvince = (e) => setProvince(e.target.value)
  const handleSetUser = (e) => setUserName(e.target.value)
  const handleSetNickname = (e) => setNickname(e.target.value)
  const handleSetPassword = (e) => setPassword(e.target.value)
  const handleSetConfirmPassword = (e) => setConfirmPassword(e.target.value)

  const handleFocusInput = (e) => {
    e.preventDefault()
    e.target.focus()
  }

  const signUp = (e) => {
    e.preventDefault()
    if (
      name === '' ||
      home === '' ||
      email === '' ||
      bornDate === '' ||
      country === '' ||
      location === '' ||
      province === '' ||
      userName === '' ||
      nickname === '' ||
      password === '' ||
      postalCode === '' ||
      firstLastName === '' ||
      secondLastName === '' ||
      emailConfirmed === '' ||
      confirmPassword === '') {
      toast.error('Completa los campos vacíos')
    } else {
      if (email !== emailConfirmed) {
        toast.error('Los correos no coinciden')
      } else {
        if (password !== confirmPassword) {
          toast.error('Las contraseñas no coinciden')
        } else {
          if(bornDate.charAt(13) === '') {
            toast.error('Intruzca una fecha válida')
          } else {
            const data = db.ref('preRegisterUsers').push()
            data.set({
              approved: 'waiting',
              bornDate: bornDate,
              country: country,
              email: email,
              firstLastName: firstLastName,
              home: home,
              id: data.key,
              isEmailCheck: 1,
              location: location,
              name: name,
              nickname: nickname,
              password: password,
              postalCode: postalCode,
              province: province,
              secondLastName: secondLastName,
              userName: userName
            })
            .then(() => {
              toast.success('Solicitud enviada')
              setName('')
              setFirstLastName('')
              setSecondLastName('')
              setBornDate('')
              setEmail('')
              setEmailConfirmed('')
              setCountry('')
              setHome('')
              setPostalCode('')
              setLocation('')
              setProvince('')
              setUserName('')
              setNickname('')
              setPassword('')
              setConfirmPassword('')
            })
            .catch(() => {
              toast.error('Algo salió mal')
            })
          }
        }
      }
    }
  }

  return (
    <div className="signup">
      <p>REGISTRO</p>
      <form onSubmit={signUp}>
        <div className="divForm">
          <input type="text" placeholder="Nombre" value={name} onChange={handleSetName} onClick={handleFocusInput} />
          <input type="text" placeholder="Primer apellido" value={firstLastName} onChange={handleSetFirstLastName} onClick={handleFocusInput} />
          <input type="text" placeholder="Segundo apellido" value={secondLastName} onChange={handleSecondLastName} onClick={handleFocusInput} />
          <NumberFormat format="## / ## / ####" placeholder="Fecha de nacimiento" value={bornDate} onChange={handleSetBornDate} onClick={handleFocusInput} />
        </div>
        <div className="divForm">
          <input type="text" placeholder="E-mail" value={email} onChange={handleSetEmail} onClick={handleFocusInput} />
          <input type="text" placeholder="Confirmar E-mail" value={emailConfirmed} onChange={handleSetEmailConfirmed} onClick={handleFocusInput} />
          <select value={country} onChange={handleSetCountry} onClick={handleFocusInput}>
            <Options />
          </select>
          <input type="text" placeholder="Domicilio" value={home} onChange={handleSetHome} onClick={handleFocusInput} />
        </div>
        <div className="divForm">
          <div className="cpLocation">
            <input className="cp" type="text" placeholder="CP" value={postalCode} onChange={handleSetPostalCode} onClick={handleFocusInput} />
            <input className="location" type="text" placeholder="Localidad" value={location} onChange={handleSetLocation} onClick={handleFocusInput} />
          </div>
          <input type="text" placeholder="Provincia" value={province} onChange={handlerSetProvince} onClick={handleFocusInput} />
          <input type="text" placeholder="Usuario" value={userName} onChange={handleSetUser} onClick={handleFocusInput} />
          <input type="text" placeholder="Nickname" value={nickname} onChange={handleSetNickname} onClick={handleFocusInput} />
        </div>
        <div className="passwordContainer">
          <input type="password" placeholder="Contraseña" value={password} onChange={handleSetPassword} onClick={handleFocusInput} />
          <input type="password" placeholder="Confirma la contraseña" value={confirmPassword} onChange={handleSetConfirmPassword} onClick={handleFocusInput} />
        </div>
        <button className="btnSignup" type="submit">
          Registrarme
      </button>
      </form>
    </div>
  )
}

export default Signup
import React from 'react'
import { Link } from 'react-scroll'

function Steps() {
  return (
    <div className="steps">
      <div className="stepsContainer">
        <div className="step">
          <div className="stepNumber">
            <p>1</p>
          </div>
          <div className="stepText1">
            <p className="stepTitle">DESCARGA E INSTALA NUESTRO SOFTWARE</p>
            <p className="stepText">Elige tu dispositivo y descarga el software para Windows, Mac o Android.</p>
          </div>
        </div>
        <div className="step">
          <div className="stepNumber">
            <p>2</p>
          </div>
          <div className="stepText2">
            <p className="stepTitle">REGÍSTRATE</p>
            <p className="stepText">
              Envía tu solicitud de registro
              <Link activeClass="active"
                to="header"
                spy={true}
                smooth={true}
                offset={50}
                duration={300}
                delay={200}
              >
                aquí.
              </Link>
            </p>
          </div>
        </div>
        <div className="step">
          <div className="stepNumber">
            <p>3</p>
          </div>
          <div className="stepText2">
            <p className="stepTitle">COMIENZA A JUGAR</p>
            <p className="stepText">Ingresa al lobby, elige una mesa o torneo y disfruta jugando.</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Steps
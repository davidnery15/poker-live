import React from 'react'
import { NavLink } from 'react-router-dom'
import { Link } from 'react-scroll'

function LinksDepositFooter() {
  return (
    <React.Fragment>
      <NavLink to='/'>Inicio</NavLink>
      <Link activeClass="active"
        to="deposit"
        spy={true}
        smooth={true}
        offset={50}
        duration={300}
        delay={200}>
        Operaciones
      </Link>
      <Link activeClass="active"
        to="calculator"
        spy={true}
        smooth={true}
        offset={50}
        duration={300}
        delay={200}>
        Calculadora
      </Link>
    </React.Fragment>
  )
}

export default LinksDepositFooter
import React from 'react'
import { NavLink } from 'react-router-dom'

function LinksDeposit() {
  return (
    <React.Fragment>
      <NavLink to='/'>Inicio</NavLink>
      <NavLink to='/operaciones'>Operaciones</NavLink>
    </React.Fragment>
  )
}

export default LinksDeposit
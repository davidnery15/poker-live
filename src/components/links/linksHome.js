import React from 'react'
import { NavLink } from 'react-router-dom'
import { Link } from 'react-scroll'

function LinksHome() {
  return (
    <React.Fragment>
      <Link activeClass="active"
        to="home"
        spy={true}
        smooth={true}
        offset={50}
        duration={300}
        delay={200}>
        Inicio
      </Link>
      <Link activeClass="active"
        to="downloads"
        spy={true}
        smooth={true}
        offset={50}
        duration={300}
        delay={200}>
        Descargas
      </Link>
      <NavLink to='/operaciones'>Operaciones</NavLink>
      <NavLink to='/operaciones'>Calculadora</NavLink>
      <Link activeClass="active"
        to='support'
        spy={true}
        smooth={true}
        offset={50}
        duration={300}
        delay={200}>
        Soporte
      </Link>
    </React.Fragment>
  )
}

export default LinksHome
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Link } from 'react-scroll'

function LinksHomeFooter() {
  return (
    <React.Fragment>
      <Link activeClass="active"
        to="home"
        spy={true}
        smooth={true}
        offset={50}
        duration={300}
        delay={200}>
        Inicio
      </Link>
      <Link activeClass="active"
        to="downloads"
        spy={true}
        smooth={true}
        offset={50}
        duration={300}
        delay={200}>
        Descargas
      </Link>
      <NavLink to='/operaciones'>Operaciones</NavLink>
    </React.Fragment>
  )
}

export default LinksHomeFooter
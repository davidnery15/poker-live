import React, { useState } from 'react'
import { Link } from 'react-scroll'
import { toast } from 'react-toastify'
import { loginUserUssernamePass } from "../../redux/actions/general"
import { connect } from "react-redux"
import axios from 'axios'
import Loader from 'react-loader-spinner'
import Modal from 'react-modal'
import HeaderHome from '../header/headerHome'
import Signup from '../SignUp'
import Downloads from '../downloads'
import Steps from '../steps'
import Support from '../support'
import FooterHome from '../footer/footerHome'
import HomeLogo from '../../assets/homeLogo.svg'
import DownloadNow from '../../assets/downloadNow.png'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}
Modal.setAppElement('#modal')

function Home({ dispatch, isLoggedIn, isFetching, adminCheck, type }) {
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [firstTry, setFirstTry] = useState(false)
  const [modalIsOpen, setModalIsOpen] = useState(false)

  const handleModalToggle = (e) => {
    e.preventDefault()
    setModalIsOpen(!modalIsOpen)
  }

  const handleInputChangeUsername = ({ target }) => {
    setUsername(target.value)
  }
  const handleInputChangePassword = ({ target }) => {
    setPassword(target.value)
  }

  const handleKeyPress = e => {
    if (e.key === "Enter") {
      e.preventDefault()
      handleSubmit(e)
      setUsername('')
      setPassword('')
    }
  }

  const handleSubmit = e => {
    e.preventDefault()
    if (username === '') {
      toast.error('Coloca un usuario')
    } else {
      if (password === '') {
        toast.error('Coloca una contraseña')
      } else {
        setFirstTry(true)
        dispatch(loginUserUssernamePass({ username, password }))
        setUsername('')
        setPassword('')
        axios.post('https://us-central1-poker-live-42745.cloudfunctions.net/api/v1/login',
          {
            userName: username,
            password: password
          })
          .catch(() => {
            toast.error('Usuario y/o clave incorrectos')
          })
      }
    }
  }

  return (
    <React.Fragment>
      <HeaderHome dispatch={dispatch} isLoggedIn={isLoggedIn} adminCheck={adminCheck} />
      <div className="home" id="home">
        <div className="homeBackground">
          <div className="homeContainer">
            <div className="positionHome">
              <div className="formLogin">
                {!isLoggedIn
                  ?
                  type === 'USER_LOGIN_STARTED'
                    ?
                    <Loader
                      type="ThreeDots"
                      color="#fff"
                      height={30}
                      width={70}
                    />
                    :
                    <form onSubmit={handleSubmit}>
                      <input
                        type="text"
                        placeholder="Usuario"
                        name="username"
                        disabled={isFetching}
                        onChange={handleInputChangeUsername}
                        value={username}
                      />
                      <input
                        type="password"
                        placeholder="Contraseña"
                        name="password"
                        disabled={isFetching}
                        onChange={handleInputChangePassword}
                        value={password}
                        onKeyPress={handleKeyPress}
                      />
                      <div className="loginSignUp">
                        <button
                          className="btnLogin"
                          type="submit"
                          disabled={isFetching && firstTry}
                        >
                          ENTRAR
                      </button>
                        <button
                          disabled={isFetching && firstTry}
                          className="btnSignUp"
                          onClick={handleModalToggle}
                        >
                          REGISTRO
                      </button>
                      </div>
                    </form>
                  : null
                }
                <Modal
                  isOpen={modalIsOpen}
                  onRequestClose={handleModalToggle}
                  style={customStyles}
                  className="Modal"
                  overlayClassName="Overlay"
                >
                  <Signup />
                </Modal>
              </div>
              <div className="logoDownloadNow">
                <img className="logoResponsive" src={HomeLogo} alt="homeLogo" />
                <Link activeClass="active" to="downloads" spy={true} smooth={true} offset={50} duration={300} delay={300}>
                  <img src={DownloadNow} alt="downloadNow" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Downloads />
      <Steps />
      <Support />
      <FooterHome />
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.general.isLoggedIn,
    isFetching: state.general.isFetching,
    adminCheck: state.general.adminCheck,
    type: state.general.type
  }
}
export default connect(mapStateToProps)(Home)
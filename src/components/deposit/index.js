import React, { useState, useEffect } from 'react'
import { toast } from 'react-toastify'
import { db } from '../../config/firebase'
import { connect } from "react-redux"
import NumberFormat from 'react-number-format'
import HeaderDeposit from '../header/headerDeposit'
import Banner from '../banner'
import Retirement from '../retirement'
import Calculator from '../calculator'
import Support from '../support'
import FooterDeposit from '../footer/footerDeposit'

function Deposit({ dispatch, isLoggedIn, accessToken, adminCheck }) {
  const [date, setDate] = useState('')
  const [bank, setBank] = useState('')
  const [operationNumber, setOperationNumber] = useState('')
  const [quantity, setQuantity] = useState('')
  const [rate, setRate] = useState(0)
  const [user, setUser] = useState('')

  const handleSetDate = (e) => setDate(e.target.value)
  const handleSetBank = (e) => setBank(e.target.value)
  const handleSetOperationNumber = (e) => setOperationNumber(e.target.value)
  const handleSetQuantity = (e) => setQuantity(e.target.value)

  useEffect(() => {
    db.ref('rateDeposit').once("value", (snapshot) => {
      const rateCollection = Object.values(snapshot.val())
      const rateMount = rateCollection.map(function (obj) {
        return obj.rate
      }).pop()
      const rate = parseInt(rateMount) * 1000
      setRate(rate)
    })
    db.ref(`users/${accessToken}/userName`).once("value", (snapshot) => {
      const user = snapshot.val()
      setUser(user)
    })
  }, [accessToken])

  const setForm = () => {
    setDate('')
    setBank('')
    setOperationNumber('')
    setQuantity('')
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (!isLoggedIn) {
      toast.error('Debe iniciar sesión')
      setForm()
    } else {
      if (adminCheck) {
        toast.error('No es posible realizar el depósito')
        setForm()
      } else {
        if (date === '' || bank === '' || operationNumber === '' || quantity === '') {
          toast.error('Completa los campos vacíos')
        } else {
          const mount = parseInt(quantity)
          const result = parseFloat((mount / rate).toFixed(2))
          // get date and time
          const today = new Date()
          const dayToday = today.getDate()
          const monthToday = today.getMonth() + 1
          const yearToday = parseInt(today.getFullYear().toString().substring(2, 4))
          const auditDate = `${dayToday} / ${monthToday} / ${yearToday}`
          const auditTime = `${today.getHours()} : ${today.getMinutes()}`
          // user date validation
          const dayUser = parseInt(date.substring(0, 2))
          const monthUser = parseInt(date.substring(5, 7))
          const yearUser = parseInt(date.substring(10, 14))
          if (dayUser < 1 || dayUser > 31) {
            toast.error('Introduzca un día válido')
          } else {
            if (monthUser < 1 || monthUser > 12) {
              toast.error('Introduzca un mes válido')
            } else {
              if (yearUser > yearToday) {
                toast.error('Introduzca un año válido')
              } else {
                // deposit register
                const data = db.ref('deposit').push()
                data.set({
                  approved: "waiting",
                  auditDate: auditDate,
                  auditTime: auditTime,
                  bank: bank,
                  date: date,
                  disabled: false,
                  dollarChange: result,
                  id: data.key,
                  operationNumber: operationNumber,
                  quantity: quantity,
                  user: user,
                  userId: accessToken
                })
                toast.success('Depósito registrado')
                setForm()
              }
            }
          }
        }
      }
    }
  }

  return (
    <React.Fragment>
      <HeaderDeposit dispatch={dispatch} isLoggedIn={isLoggedIn} adminCheck={adminCheck} />
      <Banner />
      <div className="deposit">
        <p>DEPÓSITO</p>
        <form onSubmit={handleSubmit}>
          <div className="divForm">
            <NumberFormat format="## / ## / ##" placeholder="Fecha del depósito" value={date} onChange={handleSetDate} />
            <input type="text" placeholder="Entidad bancaria" value={bank} onChange={handleSetBank} />
            <input type="text" placeholder="Numero de operación" value={operationNumber} onChange={handleSetOperationNumber} />
            <input type="number" placeholder="Monto Bs." value={quantity} onChange={handleSetQuantity} />
          </div>
          <button className="btnSignup" type="submit">
            REGISTRAR DEPÓSITO
          </button>
        </form>
      </div>
      <Retirement isLoggedIn={isLoggedIn} accessToken={accessToken} rate={rate} user={user} adminCheck={adminCheck} />
      <Calculator rate={rate} />
      <Support />
      <FooterDeposit />
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.general.isLoggedIn,
    accessToken: state.general.accessToken,
    adminCheck: state.general.adminCheck
  }
}
export default connect(mapStateToProps)(Deposit)
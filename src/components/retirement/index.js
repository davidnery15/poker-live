import React, { useState } from 'react'
import { toast } from 'react-toastify'
import { db } from '../../config/firebase'

function Retirement({ isLoggedIn, rate, user, accessToken, adminCheck }) {
  const [dollarMount, setDollarMount] = useState('')
  const [bsChange, setBsChange] = useState(0)
  const [bank, setBank] = useState('')
  const [account, setAccount] = useState('')
  const [owner, setOwner] = useState('')
  const [identification, setIdentification] = useState('')

  const handleSetDollarMount = (e) => {
    setDollarMount(e.target.value)
    const bs = e.target.value * rate
    const numberFormat = new Intl.NumberFormat("de-DE").format(bs)
    if (numberFormat === 'NaN') {
      setBsChange(0)
    } else {
      setBsChange(numberFormat)
    }
  }
  const handleSetBank = (e) => setBank(e.target.value)
  const handleSetAccount = (e) => setAccount(e.target.value)
  const handleSetOwner = (e) => setOwner(e.target.value)
  const handleSetIdentification = (e) => setIdentification(e.target.value)
  const setForm = () => {
    setDollarMount('')
    setBank('')
    setAccount('')
    setOwner('')
    setIdentification('')
    setBsChange(0)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (!isLoggedIn) {
      toast.error('Debe iniciar sesión')
      setForm()
    } else {
      if (adminCheck) {
        toast.error('No es posible realizar el retiro')
        setForm()
      } else {
        if (dollarMount === '' || bank === '' || account === '' || owner === '' || identification === '') {
          toast.error('Completa los campos vacíos')
        } else {
          // get date
          const today = new Date()
          const yearToday = today.getFullYear().toString().substring(2, 4)
          const auditDate = `${today.getDate()} / ${today.getMonth() + 1} / ${yearToday}`
          // get Bs
          const Bs = dollarMount * rate
          // get dollar
          const dollarMountTransform = parseInt(dollarMount).toFixed(2)
          // retirement register
          const data = db.ref('retirement').push()
          data.set({
            user: user,
            auditDate: auditDate,
            transactionDate: 'Sin procesar',
            dollarMount: dollarMountTransform,
            Bs: Bs,
            useRate: rate,
            bank: bank,
            account: account,
            owner: owner,
            identification: identification,
            disabled: false,
            approved: "waiting",
            id: data.key,
            userId: accessToken
          })
          toast.success('Retiro registrado')
          setForm()
        }
      }
    }
  }

  return (
    <div className="retirement">
      <p className="retirementTitle">SOLICITUD DE RETIRO</p>
      <form onSubmit={handleSubmit}>
        <div className="divForm">
          <input type="input" placeholder="Monto en dólares" value={dollarMount} onChange={handleSetDollarMount} />
          <div className="bsChange">
            <p>Bs.</p>
            <input type="text" value={bsChange} disabled />
          </div>
          <input type="text" placeholder="Entidad bancaria" value={bank} onChange={handleSetBank} />
        </div>
        <div>
          <input type="number" placeholder="N° de cuenta" value={account} onChange={handleSetAccount} />
          <input type="text" placeholder="Titular" value={owner} onChange={handleSetOwner} />
          <input type="number" placeholder="Cédula" value={identification} onChange={handleSetIdentification} />
        </div>
        <button className="btnSignup" type="submit">REGISTRAR RETIRO</button>
      </form>
    </div>
  )
}

export default Retirement
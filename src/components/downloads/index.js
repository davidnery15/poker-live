import React from 'react'
import DownArrow from '../../assets/downArrow.svg'
import WindwosIcon from '../../assets/windows-icon.svg'
import MacIcon from '../../assets/mac-icon.svg'
import AndroidIcon from '../../assets/android-icon.svg'

function Downloads() {
  return (
    <div className="downloads">
      <div className="arrowDownload">
        <img src={DownArrow} alt="downArrow" />
        <p className="downloadApp">DESCARGAS</p>
      </div>
      <p className="pDownload">Descárgalo ya y disfruta nuestras promociones de apertura.</p>
      <div className="downloadsIcon">
        <a href="https://newpoker.brasilpokerlive.com/iredirector?targetPageId=DOWNLOAD_WINDOWS_CLIENT">
          <img src={WindwosIcon} alt="windows-icon" />
        </a>
        <a href="https://newpoker.brasilpokerlive.com/iredirector?targetPageId=DOWNLOAD_MAC_CLIENT">
          <img src={MacIcon} alt="mac-icon" />
        </a>
        <a href="https://newpoker.brasilpokerlive.com/iredirector?targetPageId=DOWNLOAD_ANDROID_CLIENT">
          <img src={AndroidIcon} alt="android-icon" />
        </a>
      </div>
    </div>
  )
}

export default Downloads
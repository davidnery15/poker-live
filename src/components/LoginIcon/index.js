import React, { useState } from 'react'
import { userLogout } from "../../redux/actions/general"
import { NavLink } from 'react-router-dom'

function LoginIcon({ dispatch, adminCheck }) {
  const [loginToggle, setLoginToggle] = useState(false)
  const menuToggle = (e) => {
    e.preventDefault()
    setLoginToggle(!loginToggle)
  }
  const userLogOut = (e) => {
    e.preventDefault()
    dispatch(userLogout())
  }
  document.addEventListener('click', function (e) {
    const target = e.target.id
    if (target !== 'menuToggle' && target !== 'menuBtn') {
      setLoginToggle(false)
    }
  })

  return (
    <React.Fragment>
      <button className="LoginIcon" id="menuBtn" onClick={menuToggle} />
      {loginToggle
        ? <div className="menuToggleLogin" id="menuToggle">
          {adminCheck
            ? <NavLink to='/admin'>Panel</NavLink>
            : null
          }
          <button onClick={userLogOut}>
            Cerrar sesión
          </button>
        </div>
        : null
      }
    </React.Fragment>
  )
}

export default LoginIcon
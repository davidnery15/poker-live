import React from 'react'
import HomeLogo from '../../assets/homeLogo.svg'

function Banner() {
  return (
    <React.Fragment>
      <div className="banner">
        <div className="homeBackground">
          <img src={HomeLogo} alt="HomeLogo" />
        </div>
      </div>
    </React.Fragment>
  )
}

export default Banner
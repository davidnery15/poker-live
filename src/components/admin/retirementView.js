import React, { useState, useEffect } from 'react'
import { db } from '../../config/firebase'
import { toast } from 'react-toastify'

function RetirementView({ userAdmin }) {
  const [retirementCollection, setRetirementCollection] = useState([])
  const [rateDb, setRateDb] = useState([])
  const [transactionDate, setTransactionDate] = useState([])

  useEffect(() => {
    db.ref('retirement').on("value", (snapshot) => {
      if (snapshot.val()) {
        const retirementCollection = Object.values(snapshot.val())
        const retirementCollectionReverse = retirementCollection.reverse()
        setRetirementCollection(retirementCollectionReverse)
      }
    })
    db.ref('rateRetirement').once("value", (snapshot) => {
      const rateCollection = Object.values(snapshot.val()).pop()
      setRateDb(parseInt(rateCollection.rate))
    })
  }, [])

  const handleGetDate = () => {
    const today = new Date()
    const date = `${today.getDate()} / ${today.getMonth() + 1} / ${today.getFullYear()}`
    setTransactionDate(date)
  }

  const handleApproved = (id) => {
    handleGetDate()
    const data = db.ref(`retirement/${id}`)
    data.update({
      approved: "check",
      disabled: true,
      transactionDate: transactionDate,
      admin: userAdmin
    })
      .then(() => {
        toast.success('Retiro aprobado exitosamente')
      })
  }
  const handleDenied = (id) => {
    handleGetDate()
    const data = db.ref(`retirement/${id}`)
    data.update({
      approved: "cancel",
      disabled: true,
      transactionDate: transactionDate,
      admin: userAdmin
    })
      .then(() => {
        toast.success('Retiro rechazado exitosamente')
      })
  }

  return (
    <div className="depositView">
      <p>Retiros</p>
      <div className="depositTable">
        <table>
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Fecha Solicitud</th>
              <th>Dólares</th>
              <th>Bs.</th>
              <th>Procesador</th>
              <th>Cuenta</th>
              <th>Titular</th>
              <th>Cedula</th>
              <th>Estatus</th>
            </tr>
          </thead>
          <tbody>
            {
              retirementCollection.map((obj, i) => {
                return (
                  <tr key={i}>
                    <td>{obj.user}</td>
                    <td>{obj.auditDate}</td>
                    <td>{(parseInt(new Intl.NumberFormat("de-DE").format(obj.dollarMount))).toFixed(2)}</td>
                    <td>{obj.disabled
                      ? new Intl.NumberFormat().format(obj.Bs)
                      : new Intl.NumberFormat().format(obj.dollarMount * (rateDb * 1000))}
                    </td>
                    <td>{obj.bank}</td>
                    <td>{obj.account}</td>
                    <td>{obj.owner}</td>
                    <td>{obj.identification}</td>
                    <td>
                      {obj.approved === 'waiting' || obj.approved === 'check'
                        ? <button
                          className={`${obj.approved === "check" ? 'approvedActive' : 'approved'}`}
                          onClick={() => (handleApproved(obj.id))}
                          disabled={obj.disabled}
                        />
                        : null
                      }
                      {obj.approved === 'waiting' || obj.approved === 'cancel'
                        ? <button
                          className={`${obj.approved === "cancel" ? 'deniedActive' : 'denied'}`}
                          onClick={() => (handleDenied(obj.id))}
                          disabled={obj.disabled}
                        />
                        : null
                      }
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default RetirementView
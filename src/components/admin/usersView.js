import React, { useState, useEffect } from 'react'
import { db } from '../../config/firebase'
import bcrypt from 'bcrypt-nodejs'
import { toast } from 'react-toastify'

function DepositView() {
  const [usersCollection, setUsersCollection] = useState([])

  useEffect(() => {
    db.ref('preRegisterUsers').on("value", (snapshot) => {
      if (snapshot.val()) {
        const usersCollection = Object.values(snapshot.val())
        const usersCollectionReverse = usersCollection.reverse()
        setUsersCollection(usersCollectionReverse)
      }
    })
  }, [])

  const handleApprovedUser = (id) => {
    const callUser = db.ref(`preRegisterUsers/${id}`)
    callUser.update({
      approved: "check"
    })
    callUser.once("value", (snapshot) => {
      const data = snapshot.val()
      const encryptPassword = bcrypt.hashSync(data.password)
      const insertDataUser = db.ref('users').push()
      insertDataUser.set({
        bornDate: data.bornDate,
        country: data.country,
        email: data.email,
        firstLastName: data.firstLastName,
        home: data.home,
        location: data.location,
        name: data.name,
        nickname: data.nickname,
        password: encryptPassword,
        postalCode: data.postalCode,
        province: data.province,
        secondLastName: data.secondLastName,
        userName: data.userName
      })
    })
    toast.success('Usuario registrado exitosamente')
  }
  
  const handleDeniedUser = (id) => {
    const callUser = db.ref(`preRegisterUsers/${id}`)
    callUser.update({
      approved: "cancel"
    })
    toast.success('Usuario rechazado exitosamente')
  }

  return (
    <div className="depositView">
      <p>USUARIOS EN ESPERA DE REGISTRO</p>
      <div className="depositTable">
        <table>
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Primer Apellido</th>
              <th>Segundo Apellido</th>
              <th>Fecha Nacimiento</th>
              <th>Correo</th>
              <th>País</th>
              <th>Domicilio</th>
              <th>CP</th>
              <th>Localidad</th>
              <th>Provincia</th>
              <th>Usuario</th>
              <th>Nickname</th>
              <th>Contraseña</th>
              <th>Estatus</th>
            </tr>
          </thead>
          <tbody>
            {
              usersCollection.map((obj, i) => {
                return (
                  <tr key={i}>
                    <td>{obj.name}</td>
                    <td>{obj.firstLastName}</td>
                    <td>{obj.secondLastName}</td>
                    <td>{obj.bornDate}</td>
                    <td>{obj.email}</td>
                    <td>{obj.country}</td>
                    <td>{obj.home}</td>
                    <td>{obj.postalCode}</td>
                    <td>{obj.location}</td>
                    <td>{obj.province}</td>
                    <td>{obj.userName}</td>
                    <td>{obj.nickname}</td>
                    <td>{obj.password}</td>
                    <td>
                      {obj.approved === 'waiting' || obj.approved === 'check'
                        ? <button
                          className={`${obj.approved === "check" ? 'approvedActive' : 'approved'}`}
                          onClick={() => (handleApprovedUser(obj.id))}
                        />
                        : null
                      }
                      {obj.approved === 'waiting' || obj.approved === 'cancel'
                        ? <button
                          className={`${obj.approved === "cancel" ? 'deniedActive' : 'denied'}`}
                          onClick={() => (handleDeniedUser(obj.id))}
                        />
                        : null
                      }
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default DepositView
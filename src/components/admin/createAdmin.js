import React, { useState, useEffect } from 'react'
import { db } from '../../config/firebase'
import bcrypt from 'bcrypt-nodejs'
import { toast } from 'react-toastify'

function CreateAdmin() {
  const [usersCollection, setUsersCollection] = useState([])
  const [name, setName] = useState('')
  const [user, setUser] = useState('')
  const [password, setPassword] = useState('')

  useEffect(() => {
    db.ref('users').on("value", (snapshot) => {
      const usersCollection = Object.values(snapshot.val())
      setUsersCollection(usersCollection)
    })
  }, [])

  const handleSetName = (e) => setName(e.target.value)
  const handleSetUser = (e) => setUser(e.target.value)
  const handleSetPassword = (e) => setPassword(e.target.value)
  const setForm = () => {
    setName('')
    setUser('')
    setPassword('')
  }

  const handleDeleteAdmin = (id) => {
    db.ref(`users/${id}`).remove()
      .then(() => {
        toast.success('Administrador eliminado exitosamente')
      })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (name === '' || user === '' || password === '') {
      toast.error('Completa los campos vacíos')
    } else {
      const encryptPassword = bcrypt.hashSync(password)
      const createAdminDB = db.ref('users').push()
      createAdminDB.set({
        admin: true,
        id: createAdminDB.key,
        name: name,
        password: encryptPassword,
        userName: user
      })
      toast.success('Administrador registrado')
      setForm()
    }
  }

  return (
    <div className="depositView">
      <p>GESTIÓN DE ADMINISTRADORES</p>
      <form className="createAdminForm" onSubmit={handleSubmit}>
        <div>
          <input type="text" placeholder="Nombre y apellido" value={name} onChange={handleSetName} />
          <input type="text" placeholder="Usuario" value={user} onChange={handleSetUser} />
          <input type="text" placeholder="Contraseña" value={password} onChange={handleSetPassword} />
        </div>
        <button type="submit">CREAR ADMINISTRADOR</button>
      </form>
      <div className="adminTableContainer">
        <table>
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Usuario</th>
              <th>Acción</th>
            </tr>
          </thead>
          <tbody>
            {
              usersCollection.filter(obj => obj.admin && obj.userName !== 'root')
                .map((obj, i) => {
                  return (
                    <tr key={i}>
                      <td>{obj.name}</td>
                      <td>{obj.userName}</td>
                      <td>
                        <button
                          className="denied"
                          onClick={() => (handleDeleteAdmin(obj.id))}
                        />
                      </td>
                    </tr>
                  )
                })
            }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default CreateAdmin
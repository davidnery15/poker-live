import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import { db } from '../../config/firebase'
import HeaderAdmin from '../header/headerAdmin'
import Rate from './rate'
import DepositView from './depositView'
import RetirementView from './retirementView'
import UsersView from './usersView'
import CreateAdmin from './createAdmin'
import FooterAdmin from '../footer/footerAdmin'

function Admin({ dispatch, isLoggedIn, adminCheck, accessToken }) {
  const [showDeposit, setShowDeposit] = useState(true)
  const [showRetirement, setShowRetirement] = useState(false)
  const [showUsers, setShowUsers] = useState(false)
  const [showCreateAdmin, setShowCreateAdmin] = useState(false)
  const [userAdmin, setUserAdmin] = useState('')
  const [createAdmin, setCreateAdmin] = useState(false)
  const [activeEffect, setActiveEffect] = useState(true)

  useEffect(() => {
    if (activeEffect) {
      db.ref(`users/${accessToken}`).once("value", (snapshot) => {
        const AdminCollection = snapshot.val()
        setUserAdmin(AdminCollection.userName)
        setCreateAdmin(AdminCollection.createAdmin)
      })
      setActiveEffect(false)
    }
  }, [accessToken, activeEffect])

  const handleDeposit = e => {
    e.preventDefault()
    setShowDeposit(true)
    setShowRetirement(false)
    setShowUsers(false)
    setShowCreateAdmin(false)
  }
  const handleRetirement = e => {
    e.preventDefault()
    setShowDeposit(false)
    setShowRetirement(true)
    setShowUsers(false)
    setShowCreateAdmin(false)
  }
  const handleUsers = e => {
    e.preventDefault()
    setShowDeposit(false)
    setShowRetirement(false)
    setShowUsers(true)
    setShowCreateAdmin(false)
  }
  const handleCreateAdmin = e => {
    e.preventDefault()
    setShowDeposit(false)
    setShowRetirement(false)
    setShowUsers(false)
    setShowCreateAdmin(true)
  }

  if (!isLoggedIn || !adminCheck) {
    return <Redirect to='/' />
  } else {
    return (
      <React.Fragment>
        <HeaderAdmin dispatch={dispatch} isLoggedIn={isLoggedIn} adminCheck={adminCheck} />
        <div className="adminPanel">
          <p>Panel Administrativo</p>
          <div className="adminPanelSection">
            <button onClick={handleDeposit}>Depósitos</button>
            <button onClick={handleRetirement}>Retiros</button>
            <button onClick={handleUsers}>Usuarios</button>
            {createAdmin
              ? <button onClick={handleCreateAdmin}>Administrar</button>
              : null
            }
          </div>
        </div>
        <div className="rateChange">
          <Rate
            rateTable="rateDeposit"
            rateTitle="Tasa: Depósito"
          />
          <Rate
            rateTable="rateRetirement"
            rateTitle="Tasa: Retiro"
          />
        </div>
        {showDeposit
          ? <DepositView />
          : showRetirement
            ? <RetirementView userAdmin={userAdmin} />
            : showUsers
              ? <UsersView />
              : showCreateAdmin
                ? <CreateAdmin />
                : null
        }
        <FooterAdmin />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.general.isLoggedIn,
    accessToken: state.general.accessToken,
    adminCheck: state.general.adminCheck
  }
}

export default connect(mapStateToProps)(Admin)
import React, { useState, useEffect } from 'react'
import { db } from '../../config/firebase'
import { toast } from 'react-toastify'

function DepositView() {
  const [depositCollection, setDepositCollection] = useState([])

  useEffect(() => {
    db.ref('deposit').on("value", (snapshot) => {
      if (snapshot.val()) {
        const depositCollection = Object.values(snapshot.val())
        const depositCollectionReverse = depositCollection.reverse()
        setDepositCollection(depositCollectionReverse)
      }
    })
  }, [])

  const handleApproved = (id) => {
    const data = db.ref(`deposit/${id}`)
    data.update({
      approved: "check",
      disabled: true
    })
      .then(() => {
        toast.success('Depósito aprobado exitosamente')
      })
  }
  const handleDenied = (id) => {
    const data = db.ref(`deposit/${id}`)
    data.update({
      approved: "cancel",
      disabled: true
    })
      .then(() => {
        toast.success('Depósito rechazado exitosamente')
      })
  }

  return (
    <div className="depositView">
      <p>DEPÓSITOS</p>
      <div className="depositTable">
        <table>
          <thead>
            <tr>
              <th>Usuario</th>
              <th>Fecha Solicitud</th>
              <th>Fecha Transacción</th>
              <th>Entidad</th>
              <th>Nº de Operación</th>
              <th>Monto Bs.</th>
              <th>Monto $</th>
              <th>Estatus</th>
            </tr>
          </thead>
          <tbody>
            {
              depositCollection.map((obj, i) => {
                return (
                  <tr key={i}>
                    <td>{obj.user}</td>
                    <td>{obj.auditDate} - {obj.auditTime}</td>
                    <td>{obj.date}</td>
                    <td>{obj.bank}</td>
                    <td>{obj.operationNumber}</td>
                    <td>{new Intl.NumberFormat().format(obj.quantity)}</td>
                    <td>{new Intl.NumberFormat("de-DE").format(obj.dollarChange)}</td>
                    <td>
                      {obj.approved === 'waiting' || obj.approved === 'check'
                        ? <button
                          className={`${obj.approved === "check" ? 'approvedActive' : 'approved'}`}
                          onClick={() => (handleApproved(obj.id))}
                          disabled={obj.disabled}
                        />
                        : null
                      }
                      {obj.approved === 'waiting' || obj.approved === 'cancel'
                        ? <button
                          className={`${obj.approved === "cancel" ? 'deniedActive' : 'denied'}`}
                          onClick={() => (handleDenied(obj.id))}
                          disabled={obj.disabled}
                        />
                        : null
                      }
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default DepositView
import React, { useState, useEffect } from 'react'
import { db } from '../../config/firebase'
import NumberFormat from 'react-number-format';
import { toast } from 'react-toastify'

function Rate({ rateTable, rateTitle }) {
  const [rate, setRate] = useState('')
  const [showRate, setShowRate] = useState('')
  const [showRateDate, setShowRateDate] = useState('')
  const [showRateTime, setShowRateTime] = useState('')

  const handleSetRate = (e) => setRate(e.target.value)

  useEffect(() => {
    db.ref(`${rateTable}`).once("value", (snapshot) => {
      const rateCollection = Object.values(snapshot.val()).pop()
      setShowRate(rateCollection.rate)
      setShowRateDate(rateCollection.date)
      setShowRateTime(rateCollection.time)
    })
  }, [rateTable])

  const uptadeRate = (e) => {
    e.preventDefault()
    if (rate === '') {
      toast.error('Coloca un monto')
    } else {
      const today = new Date()
      const date = `${today.getDate()} / ${today.getMonth() + 1} / ${today.getFullYear()}`
      const time = `${today.getHours()} : ${today.getMinutes()}`
      setShowRate(rate)
      setShowRateDate(date)
      setShowRateTime(time)
      setRate('')
      const data = db.ref(`${rateTable}`)
      data.push({
        date: date,
        rate: rate,
        time: time
      })
        .then(() => {
          toast.success('Tasa actualizada exitosamente')
        })
    }
  }

  return (
    <div className="rate">
      <div className="showRate">
        <p className="rateTitle">{rateTitle}</p>
        <div>
          <pre><b>Tasa actual:</b>   {showRate} Bs/$</pre>
          <pre><b>Fecha de tasa:</b>   {showRateDate}   -   {showRateTime}</pre>
        </div>
      </div>
      <form className="rateContainer" onSubmit={uptadeRate}>
        <NumberFormat thousandSeparator={true} placeholder="Bs./$" value={rate} onChange={handleSetRate} />
        <button type="submit">ACTUALIZAR TASA</button>
      </form>
    </div>
  )
}

export default Rate
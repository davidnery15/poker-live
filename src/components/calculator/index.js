import React, { useState, useEffect } from 'react'
import { db } from '../../config/firebase'
import CalcIcon from '../../assets/calculator.svg'

function Calculator() {
  const [dollar, setDollar] = useState(1)
  const [bs, setBs] = useState(0)
  const [rate, setRate] = useState(0)

  useEffect(() => {
    db.ref('rateDeposit').once("value", (snapshot) => {
      const rateCollection = Object.values(snapshot.val())
      const rateArray = rateCollection.map(
        function (obj) {
          return obj.rate
        }
      ).pop()
      const rateMount = parseInt(rateArray) * 1000
      setRate(rateMount)
      setBs(rateMount)
    })
  }, [])

  const handleChangeBs = (e) => {
    if (e.target.value === '') {
      setBs(0)
    } else {
      const bs = parseInt(e.target.value)
      setBs(bs)
      const dollar = (parseInt(e.target.value) / rate).toFixed(2)
      setDollar(dollar)
    }
  }
  const handleChangeDollar = (e) => {
    if (e.target.value === '') {
      setDollar(0)
      setBs(0)
    } else {
      const dollar = parseFloat(e.target.value)
      setDollar(dollar)
      const bs = (parseFloat(e.target.value) * rate)
      setBs(bs)
    }
  }

  return (
    <div className="calculator">
      <div className="iconTitle">
        <img src={CalcIcon} alt="Headset" />
        <p>CALCULADORA</p>
      </div>
      <form className="rateInputContainer">
        <div>
          <p>Bs/VES</p>
          <input type="number" value={bs} onChange={handleChangeBs} />
        </div>
        <div>
          <p>$/USD</p>
          <input type="number" value={dollar} onChange={handleChangeDollar} />
        </div>
      </form>
    </div>
  )
}

export default Calculator
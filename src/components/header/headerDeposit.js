import React, { useState, } from 'react'
import { userLogout } from "../../redux/actions/general"
import { NavLink } from 'react-router-dom'
import LinksDeposit from '../links/linksDeposit'
import LoginIcon from '../LoginIcon'
import Logo from '../../assets/logo.svg'
import Menu from '../../assets/icon-menu.svg'

function HeaderDeposit({ dispatch, isLoggedIn, adminCheck }) {
  const [showMenu, setShowMenu] = useState(false)
  const menuToggle = () => setShowMenu(!showMenu)
  const userLogOut = (e) => {
    e.preventDefault()
    dispatch(userLogout())
  }
  document.addEventListener('click', function (e) {
    const target = e.target.id
    if (target !== 'menuToggle' && target !== 'menuBtn') {
      setShowMenu(false)
    }
  })

  return (
    <div className="headerContainer">
      <header className="header">
        <NavLink to='/'>
          <img src={Logo} alt="logo" id="holaLogo" />
        </NavLink>
        <nav className="nav">
          <div className="linksContainer">
            <LinksDeposit />
            {isLoggedIn
              ? <LoginIcon dispatch={dispatch} adminCheck={adminCheck} />
              : null
            }
          </div>
          <button className="menuBtn" onClick={menuToggle}>
            <img src={Menu} alt="menu" id="menuBtn" />
          </button>
          {showMenu
            ? <div className="menuToggle" id="menuToggle">
              <NavLink to='/'>Inicio</NavLink>
              {isLoggedIn
                ? <React.Fragment>
                  {adminCheck
                    ? <NavLink href="/admin">Panel</NavLink>
                    : null
                  }
                  <button className="logoutButton" onClick={userLogOut}>
                    Cerrar sesión
                </button>
                </React.Fragment>
                : null
              }
            </div>
            : null
          }
        </nav>
      </header>
    </div>
  )
}

export default HeaderDeposit
import React, { useState, } from 'react'
import { userLogout } from "../../redux/actions/general"
import { NavLink } from 'react-router-dom'
import { Redirect } from 'react-router'
import LinksAdmin from '../links/linksAdmin'
import LoginIcon from '../LoginIcon'
import Logo from '../../assets/logo.svg'
import Menu from '../../assets/icon-menu.svg'

function HeaderAdmin({ dispatch, isLoggedIn, adminCheck }) {
  const [showMenu, setShowMenu] = useState(false)
  const [redirect, setRedirect] = useState(false)
  const menuToggle = () => setShowMenu(!showMenu)
  const userLogOut = (e) => {
    e.preventDefault()
    dispatch(userLogout())
    setRedirect(!redirect)
  }
  if (redirect) {
    return <Redirect to='/' />
  }
  document.addEventListener('click', function (e) {
    const target = e.target.id
    if (target !== 'menuToggle' && target !== 'menuBtn') {
      setShowMenu(false)
    }
  })

  return (
    <div className="headerContainer">
      <header className="header">
        <NavLink to='/'>
          <img src={Logo} alt="logo" id="holaLogo" />
        </NavLink>
        <nav className="nav">
          <div className="linksContainer headerAdmin">
            <LinksAdmin />
            {isLoggedIn
              ? <LoginIcon dispatch={dispatch} adminCheck={adminCheck} />
              : null
            }
          </div>
          <button className="menuBtn" onClick={menuToggle}>
            <img src={Menu} alt="menu" id="menuBtn" />
          </button>
          {showMenu
            ? <div className="menuToggle" id="menuToggle">
              <LinksAdmin />
              {isLoggedIn
                ? <button className="logoutButton" onClick={userLogOut}>
                  Cerrar sesión
                </button>
                : null
              }
            </div>
            : null
          }
        </nav>
      </header>
    </div>
  )
}

export default HeaderAdmin
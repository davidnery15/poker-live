import React from 'react'
import LogoFooter from '../../assets/logoFooter.svg'

function FooterAdmin() {
  return (
    <footer className="footerAdmin">
      <img src={LogoFooter} alt="LogoFooter" />
    </footer>
  )
}

export default FooterAdmin
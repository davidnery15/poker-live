import React from 'react'
import LinksHomeFooter from '../links/linksHomeFooter'
import LogoFooter from '../../assets/logoFooter.svg'

function Footer() {
  return (
    <footer>
      <div className="links">
        <LinksHomeFooter />
      </div>
      <p>
        De Las Américas con NIF A73972010 y domicilio en Carlos V, 16, 1B. 52006 Melilla cuenta con las licencias número 354/GO/1075, 447/BLJ/1075, 448/MAZ/1075, 445/POQ/1075, 446/RLT/1075 otorgadas por el Gobierno de España y que están reguladas por la Dirección General de la Ordenación del Juego en España.
      </p>
      <img src={LogoFooter} alt="LogoFooter" />
    </footer>
  )
}

export default Footer
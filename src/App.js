import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import './scss/styles.scss'
import Home from './components/home'
import Deposit from './components/deposit'
import Admin from './components/admin'

function App() {
  return (
    <React.Fragment>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnVisibilityChange
        draggable
        pauseOnHover
      />
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/operaciones' component={Deposit} />
          <Route path='/admin' component={Admin} />
        </Switch>
      </BrowserRouter>
    </React.Fragment>
  )
}
export default App